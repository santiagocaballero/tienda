from django.db import models
from .user import User

from .state import State
from .precio import Precio
from .sabor import Sabor

class Jabon(models.Model):

    id_jabon = models.AutoField(primary_key=True)
   
    sabor = models.CharField('Sabor', max_length = 256)
    
    price = models.IntegerField(default=0)
   
    
    
