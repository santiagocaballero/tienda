from django.db import models

class State(models.Model):

    id_state = models.AutoField(primary_key=True)
    name_state = models.CharField('State', max_length = 30)