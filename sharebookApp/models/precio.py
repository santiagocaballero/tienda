from django.db import models

class Precio(models.Model):

    id_precio = models.AutoField(primary_key=True)
    precio = models.CharField('Precio', max_length = 30)