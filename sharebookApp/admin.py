from django.contrib import admin
from .models.user import User
from .models.jabon import Jabon
from .models.precio import Precio
from .models.state import State
from .models.sabor import Sabor


# Register your models here.
admin.site.register(User)
admin.site.register(Jabon)
admin.site.register(Precio)
admin.site.register(State)
admin.site.register(Sabor)

